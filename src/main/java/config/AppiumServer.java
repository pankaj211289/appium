package config;

import java.io.IOException;
import java.net.ServerSocket;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

public class AppiumServer implements Runnable {
	private String address;
	private String port;
	private AppiumDriverLocalService service;
	
	public AppiumServer(String address, String port){
		this.address = address;
		this.port = port;
	}
	
	@Override
	public void run() {
		runServer(address, port);
		synchronized (this) {
			notify();
		}
	}
	
	public void runServer(String address, String port) {
		if(!checkIfServerIsRunnning(Integer.parseInt(port))) {
			DesiredCapabilities capabilities = new DesiredCapabilities();
			AppiumServiceBuilder appiumServiceBuilder = new AppiumServiceBuilder();
			
			capabilities.setCapability("noReset", "false");
			
			//Build the Appium service
			appiumServiceBuilder.withIPAddress(address);
			appiumServiceBuilder.usingPort(Integer.parseInt(port));
			appiumServiceBuilder.withCapabilities(capabilities);
			appiumServiceBuilder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
			appiumServiceBuilder.withArgument(GeneralServerFlag.LOG_LEVEL,"error");
			
			service = AppiumDriverLocalService.buildService(appiumServiceBuilder);
			service.start();
			System.out.println("Appium server is started on port : " + port);
		}
	}
	
	public boolean checkIfServerIsRunnning(int port) {
		boolean isServerRunning = false;
		ServerSocket serverSocket;
		try {
			serverSocket = new ServerSocket(port);
			serverSocket.close();
		} catch (IOException e) {
			System.out.println("Already Running");
			isServerRunning = true;
		} finally {
			serverSocket = null;
		}
		return isServerRunning;
	}	
	
	public void stopServer() {
		System.out.println("Stopped Appium server...");
		service.stop();
	}
	
}

package config;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import util.PropertiesReader;

public class Driver {
	
	public static Driver driver = new Driver();
	private static PropertiesReader propertiesReader = new PropertiesReader();
	
	// Let's not to instantiate the class
	private Driver() {}
	
	private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>() {
		@Override
		public WebDriver initialValue() {
			try {
				return initAppiumDriver(OS.ANDROID);
			} catch (MalformedURLException e) {
				e.printStackTrace();
				return null;
			}
		}
	};
	
	private static WebDriver initAppiumDriver(OS os) throws MalformedURLException {
		AndroidDriver<?> androidDriver;
		DesiredCapabilities capabilities = new DesiredCapabilities();
		
		if(os == OS.ANDROID) {
	        capabilities.setCapability("deviceName", "Android");
	        capabilities.setCapability(CapabilityType.BROWSER_NAME, "Android");
	        capabilities.setCapability(CapabilityType.VERSION, "6.0.1");
	        capabilities.setCapability("autoGrantPermissions", "true");
	        capabilities.setCapability("platformName", "Android");
	        capabilities.setCapability("appPackage", "com.box.android");
	        capabilities.setCapability("appActivity", "com.box.android.activities.login.SplashScreenActivity");
		} else if(os == OS.IOS) {
			// Still need to put stub regarding IOS
		}

		String ip = propertiesReader.read("ip");
		String port = propertiesReader.read("port");
		androidDriver = new AndroidDriver<>(new URL("http://" + ip + ":" + port + "/wd/hub"), capabilities);
		androidDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return androidDriver;
	}
	
	public static WebDriver getInstance() {
		return webDriver.get();
	}
	
	public static void close(WebDriver driver) {
		webDriver.remove();
	}
	
	enum OS {
		IOS, ANDROID;
	}
}

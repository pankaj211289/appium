package pages.files;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import pages.home.HomePage;
import util.BasicActions;

public class UploadFile {
	
	private By titleBy = By.id("android:id/title");
	private By optionsBy = By.xpath("//android.widget.ImageButton[@content-desc='Show roots']");
	
	private By menuSearchIconBy = By.id("com.android.documentsui:id/menu_search");
	private By searchAreaBy = By.id("search_src_text");
	private By openSelectedFileBy = By.id("com.android.documentsui:id/menu_open");
	
	private HomePage homePage = new HomePage();

	public void uploadFileFromGoogleDrive(String itemName) {
		BasicActions.click(optionsBy);
		clickTitle("Drive");
		BasicActions.click(menuSearchIconBy);
		BasicActions.enterTextReturn(searchAreaBy, itemName);
		clickAtTitle(itemName);
		BasicActions.click(openSelectedFileBy);
		homePage.waitUntilItem(itemName);
	}
	
	public void clickTitle(String title) {
		for(WebElement we : BasicActions.getMultiple(titleBy)) {
			if(we.getText().trim().equals(title)) {
				BasicActions.click(we);
			}
		}
	}
	
	public void clickAtTitle(String title) {
		for(WebElement we : BasicActions.getMultiple(titleBy)) {
			if(we.getText().trim().equals(title)) {
				BasicActions.clickAt(we, we.getLocation().getX(), we.getLocation().getY());
			}
		}
	}
}

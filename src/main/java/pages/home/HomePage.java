package pages.home;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import pages.home.actiondata.DeleteOperationData;
import util.BasicActions;

public class HomePage {
	
	private By addFilesButtonBy = By.xpath("//android.widget.ImageButton[@index='0']");
	private By addFromLibraryBy = By.id("fab_library");
	private By selectFilesToUploadBy = By.xpath("//android.widget.TextView[@text='Select files to upload']");
	
	private By boxItemsBy = By.id("boxItemSelectedLayout");
	private By boxItemNameBy = By.id("box_browsesdk_name_text");
	private By boxItemActionBy = By.id("secondaryAction"); 
	private By itemOptionsBy = By.id("design_bottom_sheet");
	private By itemOptionTitleBy = By.id("title");
	private By itemDeleteDialogBtnBy = By.id("btnOK");
	
	private By renameEditDialogBy = By.id("dialog_edit_text");
	private By renameEditDialogOKBtnBy = By.id("btnOK");
	
	public void waitUntilItem(String itemName) {
		BasicActions.waitUntilItem(boxItemNameBy, itemName);
	}
	
	public void clickAddButtonAndSelect(HomeAddOptions option) {
		BasicActions.click(addFilesButtonBy);
		
		switch (option) {
			case ADD_FROM_LIBRARY:
				BasicActions.click(addFromLibraryBy);
				BasicActions.click(selectFilesToUploadBy);
			break;

		default:
			break;
		}
	}

	public void deleteItem(String itemName) {
		performActionOnItem(itemName, ItemAction.DELETE);
	}
	
	public void renameItem(String itemName, String updatedName) {
		ItemAction itemAction = ItemAction.RENAME;
		((DeleteOperationData)itemAction.getItemActionData()).setUpdatedName(updatedName);
		performActionOnItem(itemName, itemAction);
	}
	
	public void performActionOnItem(String itemName, ItemAction itemAction) {
		for(WebElement item : BasicActions.getMultiple(boxItemsBy)) {
			WebElement itemNameEle = item.findElement(boxItemNameBy);
			if(itemNameEle.getText().trim().equals(itemName)) {
				BasicActions.click((WebElement)item.findElement(boxItemActionBy));
				break;
			}
		}
		
		List<WebElement> options = BasicActions.getMultipleFromWithin(itemOptionsBy, itemOptionTitleBy);
		BasicActions.swipe(options.get(3), options.get(0));
		
		options = BasicActions.getMultipleFromWithin(itemOptionsBy, itemOptionTitleBy);
		for(WebElement option : options) {
			if(option.getText().trim().equals(itemAction.getItemActionTitle())) {
				BasicActions.click(option);
				break;
			}
		}
		
		switch (itemAction) {
			case SHARE:
				
				break;
			case ADD_TO_FAVORITES:
				
				break;
			case MAKE_AVAILABLE_OFFLINE:
				
				break;
			case DOWNLOAD:
				
				break;
			case UPLOAD_NEW_VERSION:
				
				break;
			case RENAME:
				BasicActions.clearText(renameEditDialogBy);
				BasicActions.enterText(renameEditDialogBy, ((DeleteOperationData)itemAction.getItemActionData()).getUpdatedName());
				BasicActions.click(renameEditDialogOKBtnBy);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				break;
			case COPY_MOVE:
				
				break;
			case DELETE:
				BasicActions.click(itemDeleteDialogBtnBy);
				break;
			case FILE_INFORMATION:
				
				break;
			
			default:
				break;
		}
		
	}
	
	enum ItemAction {
		SHARE("Share", null), 
		ADD_TO_FAVORITES("Add to Favorites", null), 
		MAKE_AVAILABLE_OFFLINE("Make available offline", null), 
		DOWNLOAD("Download", null), 
		UPLOAD_NEW_VERSION("Upload new version", null), 
		RENAME("Rename", new DeleteOperationData()), 
		COPY_MOVE("Copy / Move", null),
		DELETE("Delete", null),
		FILE_INFORMATION("File Information", null);
		
		private String itemActionTitle;
		private ItemActionData itemActionData;
		
		private ItemAction(String itemActionTitle, ItemActionData actiondata) {
			this.itemActionTitle = itemActionTitle;
			this.itemActionData = actiondata;
		}
		
		public String getItemActionTitle() {
			return itemActionTitle;
		}
		
		public ItemActionData getItemActionData() {
			return itemActionData;
		}
	}

	public enum HomeAddOptions {
		ADD_FROM_LIBRARY("Add From Library"),
		NEW_MEDIA("New Media"),
		NEW_BOX_NOTE("New Box Note"),
		NEW_FOLDER("New Folder");
		
		private String addOption;
		
		HomeAddOptions(String addOptions) {
			this.addOption = addOptions;
		}
		
		public String getOptionValue() {
			return addOption;
		}
	}
}

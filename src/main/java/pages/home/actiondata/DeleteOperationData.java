package pages.home.actiondata;

import pages.home.ItemActionData;

public class DeleteOperationData implements ItemActionData {

	private String updatedName;

	public String getUpdatedName() {
		return updatedName;
	}

	public void setUpdatedName(String updatedName) {
		this.updatedName = updatedName;
	}
}

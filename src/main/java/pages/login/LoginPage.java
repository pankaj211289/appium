package pages.login;

import org.openqa.selenium.By;

import box.user.UserData;
import util.BasicActions;

public class LoginPage {

	private By usernameBy = By.id("login");
	private By passwordBy = By.id("password");
	private By loginButtonBy = By.className("android.widget.Button");
	
	public void fillUserDetails(UserData userData) {
		enterUserName(userData.getUserName());
		enterPassword(userData.getPassword());
		login();
	}
	
	public void enterUserName(String username) {
		BasicActions.enterText(usernameBy, username);
	}

	public void enterPassword(String password) {
		BasicActions.enterText(passwordBy, password);
	}
	
	public void login() {
		BasicActions.click(loginButtonBy);
	}
}

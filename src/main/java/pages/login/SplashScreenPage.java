package pages.login;

import org.openqa.selenium.By;

import pages.PageObject;
import util.BasicActions;

public class SplashScreenPage extends PageObject {

	private By loginButtonId = By.id("loginButton");
	
	public void clickLoginButton() {
		BasicActions.click(loginButtonId);
	}
}

package util;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import config.Driver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;

public class BasicActions {

	private static WebDriver webDriver = Driver.getInstance();
	
	public static WebElement get(By by) {
		return webDriver.findElement(by);
	}
	
	public static List<WebElement> getMultiple(By by) {
		return webDriver.findElements(by);
	}
	
	public static void startActivity(Activity activity) {
		((AndroidDriver<?>)webDriver).startActivity(activity);
	}
	
	public static void click(By by) {
		get(by).click();
	}
	
	public static void enterText(By by, String text) {
		get(by).sendKeys(text);
	}
	
	public static void clearText(By by) {
		get(by).clear();
	}
	
	public static void enterTextReturn(By by, String text) {
		enterText(by, text);
		((AndroidDriver<?>)webDriver).pressKeyCode(66);
	}
	
	public static void click(WebElement element) {
		element.click();
	}
	
	public static void enterText(WebElement element, String text) {
		element.sendKeys(text);
	}
	
	public static void clickAt(WebElement we, int x, int y) {
		TouchAction action = new TouchAction((AppiumDriver<?>)webDriver);
		action.longPress(x, y).perform();
	}
	
	public static void swipe(WebElement from, WebElement to) {
		TouchAction action = new TouchAction((AppiumDriver<?>)webDriver);
		action.longPress(from.getLocation().getX(), from.getLocation().getY()).moveTo(to.getLocation().getX(), to.getLocation().getY()).release().perform();
	}
	
	public static void waitUntilNumOfItems(By by, int count) {
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(new ExpectedCondition<Boolean>() {

			@Override
			public Boolean apply(WebDriver input) {
				if(getMultiple(by).size() == count) 
					return true;
				return false;
			}
		});
	}
	
	public static WebElement waitUntilItem(By by, String itemName) {
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		return wait.until(new ExpectedCondition<WebElement>() {

			@Override
			public WebElement apply(WebDriver driver) {
				for(WebElement we : getMultiple(by)) {
					if(we.getText().trim().equals(itemName))
						return we;
				}
				return null;
			}
		});
	}
	
	public static List<WebElement> getMultipleFromWithin(By parentBy, By withinElements) {
		return get(parentBy).findElements(withinElements);
	}
}

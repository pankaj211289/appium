package util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {

	private Properties properties = new Properties();
	
	public String read(String key) {
		try {
			InputStream input = new FileInputStream("config.properties");
			properties.load(input);
			return properties.getProperty(key);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}

public class MultiThreadTest {

	public static void main(String[] args) throws InterruptedException {
		Operation op = new Operation();
		new Thread(op).start();
		
		synchronized (op) {
			op.wait();
			System.out.println("Done with partial op");
		}
		
		System.out.println("1");
		System.out.println("2");
	}
}

class Operation implements Runnable {
	@Override
	public void run() {
		try {
			Thread.sleep(10000);
			System.out.println("Running main thread...");
			synchronized (this) {
				notify();
			}

			System.out.println("Continue with operation thread...");
			Thread.sleep(10000);
			System.out.println("Completed thread: operation thread...");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
}

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dell on 10/28/2017.
 */

public class TestCal2 {
    AppiumDriver<?> driver;

    @BeforeClass
    public void setUp() throws MalformedURLException {
        // Created object of DesiredCapabilities class.
        DesiredCapabilities capabilities = new DesiredCapabilities();

        // Set android deviceName desired capability. Set your device name.
        capabilities.setCapability("deviceName", "ZY223Z3XSS");

        // Set BROWSER_NAME desired capability. It's Android in our case here.
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "Android");

        // Set android VERSION desired capability. Set your mobile device's OS version.
        capabilities.setCapability(CapabilityType.VERSION, "6.0.1");

        // Set android platformName desired capability. It's Android in our case here.
        capabilities.setCapability("platformName", "Android");

        // Set android appPackage desired capability. It is
        // com.android.calculator2 for calculator application.
        // Set your application's appPackage if you are using any other app.
        capabilities.setCapability("appPackage", "com.google.android.calculator");

        // Set android appActivity desired capability. It is
        // com.android.calculator2.Calculator for calculator application.
        // Set your application's appPackage if you are using any other app.
        capabilities.setCapability("appActivity", "com.android.calculator2.Calculator");

        // Created object of RemoteWebDriver will all set capabilities.
        // Set appium server address and port number in URL string.
        // It will launch calculator app in android device.
        driver = new AppiumDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testFirstCalculator() throws InterruptedException {
//    	driver.findElement(By.xpath("//android.widget.EditText[@text='Password Password']")).click();
//    	driver.findElement(By.id("login")).click();
    	
    	Thread.sleep(10000);
    	
//    	driver.findElement(By.xpath("//android.widget.EditText[contains(@text,'Email Address')]")).sendKeys("pankaj211289@gmail.com");
//        driver.findElement(By.id("password")).sendKeys("pankaj_88");
    	
//        driver.findElement(By.id("login")).sendKeys("pankaj211289@gmail.com");
//        driver.findElement(By.id("password")).sendKeys("pankaj_88");
        driver.findElementById("digit_8").click();
        driver.findElementById("com.google.android.calculator:id/digit_8").click();
        driver.findElementById("digit_8").click();
    }

    @AfterClass
    public void end() {
        driver.quit();
    }
}

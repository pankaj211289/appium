import config.AppiumServer;

public class TestServer {

	public static void main(String[] args) throws InterruptedException {
		AppiumServer appiumServer = new AppiumServer("127.0.0.1", "4723");
		new Thread(appiumServer).start();
		
		synchronized (appiumServer) {
			appiumServer.wait();
			System.out.println("Done with partial op");
		}
		
		System.out.println("1");
		System.out.println("2");
		appiumServer.stopServer();
	}
}

package box;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import config.AppiumServer;
import config.Driver;
import util.PropertiesReader;

public class BasicInit {
	
	private PropertiesReader propertiesReader = new PropertiesReader();
	private AppiumServer appiumServer = null;
	
	public BasicInit() {
		appiumServer = new AppiumServer(propertiesReader.read("ip"), propertiesReader.read("port"));
		new Thread(appiumServer).start();
		
		synchronized (appiumServer) {
			try {
				appiumServer.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	@BeforeClass
	public void setUp() throws InterruptedException {
		
	}
	
	@AfterClass
	public void cleanUp() {
		appiumServer.stopServer();
		Driver.close(Driver.getInstance());
	}
}

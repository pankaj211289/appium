package box.login;

import org.testng.annotations.Test;

import box.BasicInit;
import box.user.UserData;
import pages.files.UploadFile;
import pages.home.HomePage;
import pages.home.HomePage.HomeAddOptions;
import pages.login.LoginPage;
import pages.login.SplashScreenPage;

public class FileTest extends BasicInit {
	
	private SplashScreenPage splashScreenPage = new SplashScreenPage();
	private LoginPage loginPage = new LoginPage();
	private HomePage homePage = new HomePage();
	private UploadFile fileUpload = new UploadFile();
	
	private String fileExt = ".txt";
	private String itemName = "testFile.txt";
	private String updatedName = "123Updated";
	
	@Test
	public void uploadFile() {
		splashScreenPage.clickLoginButton();
		loginPage.fillUserDetails(UserData.USER1);
		
		homePage.clickAddButtonAndSelect(HomeAddOptions.ADD_FROM_LIBRARY);
		fileUpload.uploadFileFromGoogleDrive(itemName);
	}
	
	@Test(dependsOnMethods = "uploadFile")
	public void renameFile() {
		homePage.renameItem(itemName, updatedName);
	}
	
	@Test(dependsOnMethods = "renameFile")
	public void deleteFile() {
		homePage.deleteItem(updatedName + fileExt);
	}
}

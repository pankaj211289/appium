package box.user;

public enum UserData {
	USER1("pankaj211289@gmail.com", "pankaj_88");
	
	private String username;
	private String password;
	
	UserData(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public String getUserName() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
}
